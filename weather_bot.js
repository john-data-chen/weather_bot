'use strict'
var _l = require('lodash')
var async = require('async')
var moment = require('moment')
var fs = require('fs')
var fse = require('fs-extra')
var xml2js = require('xml2js')
var Schedule = require('node-schedule')
var request = require('request')

// disable log
console.log = function () {}

// xml url
var earthquake_url = 'http://opendata.cwb.gov.tw/govdownload?dataid=E-A0015-001R&authorizationkey=rdec-key-123-45678-011121314'
var typhoon_url = 'http://opendata.cwb.gov.tw/govdownload?dataid=W-C0034-001&authorizationkey=rdec-key-123-45678-011121314'
var xmlUrlArray = [earthquake_url, typhoon_url]

// bot parameters
var totalOfEachIntensity = 2

// slack channel
var diamondDogUrl = 'https://hooks.slack.com/services/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'
var nextDigitalUrl = 'https://hooks.slack.com/services/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'
var slackChArray = [diamondDogUrl, nextDigitalUrl]

// TWML publisher API
var twmlPubURL = 'http://mldev.publisher.twnextdigital.com/v1/1/ArticleDetail/Create'

var timeStamp = moment().format('X')
var issueId = moment().format('YYYYMMDD').toString()
var pubDate = moment().format('YYYY-MM-DD HH:mm:ss').toString()
var twmlTemplate =
{
  'articleId': '',
  'issueId': issueId,
  'title': '',
  'sourceType': 'tw_apple_daily_rtn_cms',
  '1x1src': 'TWAD',
  'pubDate': pubDate,
  'categoryL0Name': ['即時'],
  'categoryL0Id': ['1'],
  'categoryL1Name': ['AI'],
  'categoryL1Id': ['105'],
  'usHighlight': '',
  'introPhotos': [],
  'isPublish2DV': false,
  'video': [
    {}],
  'contentBlocks': [
    {
      'subHead': '',
      'content': '',
      'photos': [
        {
          'imageId': '',
          'imagePath': '',
          'imagePathZoom': '',
          'expireDate': '9999',
          'caption': '中央氣象局',
          'width': '',
          'height': '',
          'source': ''
        }]
    }],
  'tags': [
    {}],
  'social': {
    'facebook': 'https://tw.appledaily.com/new/realtime/' + issueId + '/',
    'shareUrl': 'https://tw.appledaily.com/new/realtime/' + issueId + '/',
    'shareImage': '',
    'twadurl': '',
    'youtubeId': ''
  },
  'audit': {
    'createdBy': 'AI',
    'updatedBy': 'AI',
    'createdDate': pubDate,
    'updatedDate': pubDate,
    'triggerAction': 'TWAD RTN BOT'
  },
  'actionDate': timeStamp,
  'timestamp': timeStamp
}

// mkdir if not exists
var mkDir = function () {
  try {
    if (!fse.existsSync('./xml')) {
      fse.mkdirSync('./xml')
    }
    if (!fse.existsSync('./old_xml')) {
      fse.mkdirSync('./old_xml')
    }
  } catch (err) {
    console.error(err)
  }
}

// copy xml after used
var back_xml = function (filePath) {
  try {
    fse.copySync(filePath, './old_xml/' + filePath.split('xml/')[1])
  // console.log(filePath.split('xml/')[1] + ' backup success!')
  } catch (err) {
    console.error(err)
  }
}

var crawler = function (target) {
  mkDir()
  var url = ''
  if (target == 'earthquake') {
    url = xmlUrlArray[0]
  }
  if (target == 'typhoon') {
    url = xmlUrlArray[1]
  }
  var stream = request
    .get(url)
    .on('error', function (err) {
      console.log(err)
    })
    .pipe(fs.createWriteStream('./xml/' + target + '.xml'))

  stream.on('finish', function () {
    var parser = new xml2js.Parser()
    fs.readFile('./xml/' + target + '.xml', function (err, data) {
      parser.parseString(data, function (err, xmlToJS) {
        // console.log(JSON.stringify(xmlToJS))
        fs.stat('./old_xml/' + target + '.xml', function (err, stat) {
          if (err == null) {
            // console.log('./old_xml/' + target + '.xml exists...')
            fs.readFile('./old_xml/' + target + '.xml', function (err, oldData) {
              parser.parseString(oldData, function (err, oldXml) {
                if (err) {
                  console.log('err: ', err)
                }
                back_xml('./xml/' + target + '.xml')
                if (target == 'earthquake') {
                  earthquake(xmlToJS, oldXml, function (err, status) {
                    if (err) {
                      console.log(err)
                      return
                    }
                  // console.log(target + ' status: ' + status)
                  })
                }
                if (target == 'typhoon') {
                  typhoon(xmlToJS, oldXml, function (err, status) {
                    if (err) {
                      console.log(err)
                      return
                    }
                  // console.log(target + ' status: ' + status)
                  })
                }
              })
            })
          } else if (err.code == 'ENOENT') {
            // file does not exist
            back_xml('./xml/' + target + '.xml')
            if (target == 'earthquake') {
              earthquake(xmlToJS, oldXml, function (err, status) {
                if (err) {
                  console.log(err)
                  return
                }
              // console.log(target + ' status: ' + status)
              })
            }
            if (target == 'typhoon') {
              typhoon(xmlToJS, oldXml, function (err, status) {
                if (err) {
                  console.log(err)
                  return
                }
              // console.log(target + ' status: ' + status)
              })
            }
          } else {
            console.log('Some other error: ', err.code)
            back_xml('./xml/' + target + '.xml')
            if (target == 'earthquake') {
              earthquake(xmlToJS, oldXml, function (err, status) {
                if (err) {
                  console.log(err)
                  return
                }
              // console.log(target + ' status: ' + status)
              })
            }
            if (target == 'typhoon') {
              typhoon(xmlToJS, oldXml, function (err, status) {
                if (err) {
                  console.log(err)
                  return
                }
              // console.log(target + ' status: ' + status)
              })
            }
          }
        })
      })
    })
  })
}

var slackPusher = function (final_text) {
  _l.forEach(slackChArray, function (channelUrl) {
    request.post({
      headers: {
        'content-type': 'application/json'
      },
      uri: channelUrl,
      body: JSON.stringify({
        'text': final_text
      })
    }, function (err, response, body) {
      if (err) {
        console.log('pushing to slack has error: ', err)
      }
      console.log(channelUrl)
      console.log('pushing to slack status: ' + body)
    })
  })
}

var mlPusher = function (finalJson) {
  var options = {
    uri: twmlPubURL,
    method: 'POST',
    json: {
      'data': finalJson
    }
  }
  request(options, function (error, response, body) {
    if (error) {
      console.log(body)
    }
  })
}

var earthquake = function (xmlToJS, oldXml, callback) {
  // modify earthquakeNo to force push
  if (typeof (oldXml) == 'undefined' || oldXml.cwbopendata.dataset[0].earthquake[0].earthquakeNo[0] != xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeNo[0]) {
    async.waterfall([
      function (callback) {
        var result = {}
        result.hour = moment(xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].originTime[0]).hour()
        // sync with Dev server timezone
        // result.hour += 8
        // console.log('hour: ', result.hour)

        result.minute = moment(xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].originTime[0]).minute()
        // console.log('minute: ', result.minute)

        result.location = xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[0].areaName[0]
        // console.log('location: ', result.location)

        result.magnitude = xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].magnitude[0].magnitudeValue[0]
        // console.log('magnitude: ', result.magnitude)

        result.epiLocation_1 = xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].epicenter[0].location[0].split('(')[0].replace(/\s/g, '')
        // console.log('epiLocation_1: ', result.epiLocation_1)

        result.epiLocation_2 = xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].epicenter[0].location[0].split('(')[1].replace('位於', '').replace(')', '')
        // console.log('epiLocation_2: ', result.epiLocation_2)

        result.depth = xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeInfo[0].depth[0]._
        // console.log('depth: ', result.depth)

        result.imageUrl = xmlToJS.cwbopendata.dataset[0].earthquake[0].reportImageURI[0]
        // console.log('imageUrl: ', result.imageUrl)

        callback(null, result)
      },
      function (result, callback) {
        var title = xmlToJS.cwbopendata.dataset[0].earthquake[0].reportContent[0].split('，')[0]
        callback(null, title, result)
      },
      function (title, result, callback) {
        result.title = title.split('-')[1].replace('發生', '').replace('有感地震', '') + '地震' + '  ' + '最大震度' + xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[0].areaIntensity[0]._ + '級'
        callback(null, result)
      },
      function (result, callback) {
        var shakingAreaArray = []
        for (var i = 0; i < xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea.length; i++) {
          // break when areaDesc has 最大震度
          if (xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[i].areaDesc[0].indexOf('最大震度') > -1) {
            // console.log('areaDesc: ', xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[i].areaDesc[0])
            // console.log('break')
            break
          }
          shakingAreaArray.push({
            'areaIntensity': xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[i].areaIntensity[0]._,
            'areaName': xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[i].areaName[0] + xmlToJS.cwbopendata.dataset[0].earthquake[0].intensity[0].shakingArea[i].eqStation[0].stationName[0]
          })
        }
        callback(null, shakingAreaArray, result)
      },
      function (shakingAreaArray, result, callback) {
        // console.log(JSON.stringify(shakingAreaArray, null, 2))
        var sortedArray = []
        for (var i = 0; i < shakingAreaArray.length; i++) {
          if (i == 0) {
            sortedArray.push({
              'intensity': shakingAreaArray[i].areaIntensity,
              'areas': [shakingAreaArray[i].areaName]
            })
          // console.log(JSON.stringify(sortedArray, null, 2))
          } else {
            if (shakingAreaArray[i].areaIntensity == sortedArray[sortedArray.length - 1].intensity && sortedArray[sortedArray.length - 1].areas.length < totalOfEachIntensity) {
              sortedArray[sortedArray.length - 1].areas.push(shakingAreaArray[i].areaName)
            } else {
              if (shakingAreaArray[i].areaIntensity != sortedArray[sortedArray.length - 1].intensity) {
                sortedArray.push({
                  'intensity': shakingAreaArray[i].areaIntensity,
                  'areas': [shakingAreaArray[i].areaName]
                })
              }
            }
          }
        }
        callback(null, sortedArray, result)
      },
      function (sortedArray, result, callback) {
        result.areas = ''
        for (var i = 0; i < sortedArray.length; i++) {
          for (var j = 0; j < sortedArray[i].areas.length; j++) {
            if (j == 0) {
              result.areas += sortedArray[i].areas[j]
            }else {
              result.areas += '、' + sortedArray[i].areas[j]
            }
          }
          result.areas += sortedArray[i].intensity + '級，'
        }
        // console.log(JSON.stringify(result, null, 2))
        callback(null, result)
      },
      function (result, callback) {
        // combine all parameters and output final text
        twmlTemplate.articleId = 'earthquake' + xmlToJS.cwbopendata.dataset[0].earthquake[0].earthquakeNo[0]
        twmlTemplate.imageId = twmlTemplate.articleId
        twmlTemplate.social.facebook += twmlTemplate.articleId
        twmlTemplate.social.shareUrl = twmlTemplate.facebook
        twmlTemplate.title = result.title
        twmlTemplate.contentBlocks[0].content = '地牛翻身！就在今天' + result.hour + '時' + result.minute + '分左右，' + result.location + '發生規模' + result.magnitude + '有感地震，震央位於' + result.epiLocation_1 + '處，也就是' + result.epiLocation_2 + '，地震深度' + result.depth + '公里，各地最大震度為' + result.areas.replace(/.$/, '。（生活中心／台北報導）')
        twmlTemplate.contentBlocks[0].photos[0].imagePath = result.imageUrl
        twmlTemplate.contentBlocks[0].photos[0].imagePathZoom = result.imageUrl
        twmlTemplate.social.shareImage = result.image
        var final_text = result.title + '\n' + twmlTemplate.contentBlocks[0].content + '\n' + result.imageUrl
        callback(null, final_text, twmlTemplate)
      }
    ], function (err, final_text, twmlTemplate) {
      slackPusher(final_text)
      mlPusher(twmlTemplate)
    })
  } else {
    return (callback(null, 'skip'))
  }
}

var typhoon = function (xmlToJS, oldXml, callback) {
  // modify <identifier> to force push
  if (typeof (oldXml) == 'undefined' || oldXml.alert.identifier[0] != xmlToJS.alert.identifier[0]) {
    async.waterfall([
      function (callback) {
        twmlTemplate.articleId = xmlToJS.alert.identifier[0].replace('_', '-').replace('_', '-')
        twmlTemplate.imageId = twmlTemplate.articleId
        twmlTemplate.social.facebook += twmlTemplate.articleId
        twmlTemplate.social.shareUrl = twmlTemplate.facebook
        twmlTemplate.title = '【蘋妹說天氣】' + xmlToJS.alert.info[0].senderName[0] + xmlToJS.alert.info[0].headline[0]
        twmlTemplate.contentBlocks[0].content = '以下是中央氣象局官網關於颱風警報的訊息' + xmlToJS.alert.info[0].description[0]
        var final_text = twmlTemplate.title + '\n' + twmlTemplate.contentBlocks[0].content
        callback(null, final_text, twmlTemplate)
      }
    ], function (err, final_text, twmlTemplate) {
      slackPusher(final_text)
      mlPusher(twmlTemplate)
    })
  } else {
    return (callback(null, 'skip'))
  }
}

// run only once
// crawler('earthquake')
// crawler('typhoon')

// update schedule
// update every X minutes
Schedule.scheduleJob('* * * * *', (function () { crawler('earthquake') }))
// Schedule.scheduleJob('* * * * *', (function () { crawler('typhoon') }))
